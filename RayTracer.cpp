//
// Created by luizjardim on 19.09.20.
//

#include "RayTracer.h"

#include "Image.h"
#include "Options.h"

namespace ray_tracer {

int ray_tracer(int argc, char** argv) {

    using ::ray_tracer::ImageRenderer;
    using ::ray_tracer::Options;
    using ::ray_tracer::scene::Camera;
    using ::ray_tracer::scene::Scene;
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;

    Options options;

    for (int i = 1; i < argc; i += 2) {
        std::string option_type = argv[i];
        std::string option_value = argv[i+1];
        bool success = options.add(option_type, option_value);
        if (!success) {
            std::cerr << "invalid option: " << option_type << " = " << option_value << std::endl;
            return -1;
        }
    }

    double ASPECT_RATIO = static_cast<double>(options.image_width) / options.image_height;
    Camera camera = Camera::Builder{}
            .at(Point{{1.5, -4, 2.5}})
            .withSize(ASPECT_RATIO * 2.0, 2.0)
            .withDirection(Vec{{.1, -.5, 1}})
            .withUp(Vec::J())
            .withFocalLength(1.3)
            .build();

    std::optional<Scene> scene = options.create_scene(std::move(camera));

    if (!scene) {
        std::cerr << "invalid scene name: " << options.scene_name << std::endl;
        return -1;
    }

    ImageRenderer{std::move(*scene)}
            .with_size(options.image_width, options.image_height)
            .with_rays_per_pixel(options.rays_per_pixel)
            .with_ray_bounce(options.ray_bounce)
            .with_number_of_render_threads(options.number_of_threads)
            .render()
            .gamma_correction()
            .write_to_ppm_file(options.output_file);

    return 0;
}

}
