#include "gtest/gtest.h"

#include "RayTracer.h"

#include <filesystem>

namespace fs = std::filesystem;

class CommandLineArguments {
public:
    int argc{};
    char** argv{};
    explicit CommandLineArguments(std::string const& command_line) {
        std::istringstream iss{command_line};
        std::vector<std::string> args{std::istream_iterator<std::string>{iss},
                                      std::istream_iterator<std::string>{}};
        argc = args.size();
        argv = new char*[argc];
        for (int i = 0; i < argc; ++i)
            argv[i] = const_cast<char*>(args[i].c_str());
    }
    ~CommandLineArguments() {
        delete[] argv;
    }
};

class GenerateImageTests : public ::testing::TestWithParam<std::string> {
protected:
    CommandLineArguments args{GetParam()};
};

TEST_P(GenerateImageTests, GenerateImage) {
    EXPECT_EQ(::ray_tracer::ray_tracer(args.argc, args.argv), 0);
    fs::path output_file = args.argv[args.argc -1];
    EXPECT_TRUE(fs::exists(output_file));
}

INSTANTIATE_TEST_SUITE_P(
    GenerateImageParamTests,
    GenerateImageTests,
    ::testing::Values(
        "ray_tracer --width 16 --height 9 --rays_per_pixel 3 --ray_bounce 2 --threads 9 --scene complete --output img0.ppm",
        "ray_tracer -w 16 -h 9 -r 3 -b 2 -t 9 -o img1.ppm",
        "ray_tracer -w 16 -h 9 -r 3 -b 2 -t 9 -s cylinder -o img2.ppm",
        "ray_tracer -w 16 -h 9 -r 3 -b 2 -t 9 -s ellipsoid -o img3.ppm"
    )
);

class WrongCommandTests : public ::testing::TestWithParam<std::string> {
protected:
    CommandLineArguments args{GetParam()};
};

TEST_P(WrongCommandTests, WrongCommand) {
    EXPECT_NE(::ray_tracer::ray_tracer(args.argc, args.argv), 0);
    fs::path output_file = args.argv[args.argc -1];
    EXPECT_FALSE(fs::exists(output_file));
}

INSTANTIATE_TEST_SUITE_P(
    WrongCommandParametersTests,
    WrongCommandTests,
    ::testing::Values(
        "ray_tracer --width k -output output0.ppm",
        "ray_tracer -b .4 -o output1.ppm",
        "ray_tracer -s no -o output2.ppm"
    )
);

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
