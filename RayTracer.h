//
// Created by luizjardim on 19.09.20.
//

#ifndef RAY_TRACER_RAYTRACER_H
#define RAY_TRACER_RAYTRACER_H

namespace ray_tracer {
    int ray_tracer(int argc, char** argv);
}

#endif //RAY_TRACER_RAYTRACER_H
