//
// Created by luizjardim on 08.09.20.
//

#include "Image.h"

using ::ray_tracer::Image;
using ::ray_tracer::ImageRenderer;
using Color_t = ::ray_tracer::util::Color;
using PixelLine = std::vector<Color_t>;
using PixelGrid = std::vector<PixelLine>;

Image ImageRenderer::render() {
    PixelGrid pixels = render_image();

    print_finished();

    return Image{std::move(pixels)};
}

PixelGrid ImageRenderer::render_image() {
    PixelGrid pixels;
    pixels.resize(horizontal_size);

    std::vector<std::jthread> threads;
    threads.reserve(number_of_render_threads);
    for (unsigned thread = 0; thread < number_of_render_threads; ++thread)
        threads.emplace_back([this, &pixels, thread]() {
            render_strip(pixels, thread);
        });

    return pixels;
}

void ImageRenderer::render_strip(PixelGrid& pixels, unsigned thread_number) {
    unsigned lines = horizontal_size / number_of_render_threads;
    unsigned start_line = lines * thread_number;
    unsigned end_line = start_line + lines;
    //last thread gets the remaining lines
    if (thread_number == number_of_render_threads - 1)
        end_line = horizontal_size;
    for (std::size_t i = start_line; i < end_line; ++i) {
        print_progress();
        pixels[i] = render_line(i);
    }
}

PixelLine ImageRenderer::render_line(std::size_t i) {
    PixelLine line;
    line.reserve(vertical_size);

    for (std::size_t j = 0; j < vertical_size; ++j)
        line.push_back(render_pixel(i, j));

    return line;
}

[[nodiscard]] Color_t ImageRenderer::render_pixel(std::size_t i, std::size_t j) {
    ColorAverage_t pixel_color;
    for (unsigned n = 0; n < rays_per_pixel; ++n)
        pixel_color.addColor(render_ray(i, j));
    return pixel_color.average();
}

[[nodiscard]] Color_t ImageRenderer::render_ray(std::size_t i, std::size_t j) {
    double u = (i + random.number()) / (horizontal_size - 1);
    double v = (j + random.number()) / (vertical_size - 1);
    return scene.render(u, v, ray_bounce);
}

void ImageRenderer::print_progress() {
    //alters lines_remaining in a thread-unsafe way, but I don't really care
    std::stringstream message;
    unsigned remaining_percentage = 100 - 100 * --lines_remaining / horizontal_size;
    message << "\rRendering: " << remaining_percentage << "% ";
    std::cerr << message.str() << std::flush;
}

void ImageRenderer::print_finished() {
    std::cerr << "\rRendering: finished!\n" << std::flush;
}