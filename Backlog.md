Backlog

* Fix all TODOs in the code or add them here
    * use command `grep -rIi TODO --exclude-dir={cmake-build-debug,.git,.idea} --exclude=Backlog.md`

* Add 3d object paraboloid
* Add 3d object one sheet hyperboloid
* Add 3d object two sheet hyperboloid
* Add 3d object cone (infinite)
* Add 3d object prism (blocked because it needs at least rectangle to be started)
* Add 3d object pyramid (blocked because it needs at least triangle to be started)

* Add 2d object capability (this will open the way to polyhedra)
* Add 2d object plane strip
* Add 2d object square/rectangle
* Add 2d object circle/ellipsis
* Add 2d object triangle
* Add 2d object regular polygon
* Add 2d object parabola
* Add 2d object hyperboloid
* Add 2d object plane with a hole (the hole is one of the other 2d objects)

* Translation and rotation of objects

* Test changing camera configuration (position, orientation and focus)

* Check divisions by zero (return an optional?):
    * Vec::normalized
    * Vec::operator/(T)

* Add capability for user defined background

* Refactor tests and add waaay more tests
    * add CI

* In Object::reflect, it is not being checked whether the ray is coming from inside or outside. If from inside, the normal should be flipped

* Change uses of std::size_t to something else?
    * remove `#include <array>` from Material.h

* Make focal length a length instead of a vector

* Add readme

* Add clang-format and git hook
