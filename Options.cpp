//
// Created by luizjardim on 18.09.20.
//

#include "Options.h"

#include "Albedo.h"
#include "Cilinder.h"
#include "Ellipsoid.h"
#include "FuzzyReflective.h"
#include "Image.h"
#include "Lambertian.h"
#include "Plane.h"
#include "Reflective.h"
#include "Refractive.h"
#include "Sphere.h"
#include "StraightLine.h"

using namespace ::ray_tracer;
using namespace ::ray_tracer::scene;
using Point = ::ray_tracer::util::Point<double, 3>;
using Vec = ::ray_tracer::util::Vec<double, 3>;

Scene Options::complete_scene(Camera camera) {
    using ::ray_tracer::scene::object::Object;
    using namespace ::ray_tracer::scene::object::geometry;
    using namespace ::ray_tracer::scene::object::material;
    using Line = ::ray_tracer::util::StraightLine<double, 3>;

    Scene scene{std::move(camera)};

    Albedo magenta{.50, .25, .50};
    Albedo dark_red{.50, .25, .25};
    Albedo yellow{.50, .50, .25};
    Albedo mirror{.75, .75, .75};
    Albedo dark_gay{.50, .50, .50};

    std::shared_ptr<Material> mickey_material = std::make_shared<Refractive>(mirror, 1.33, std::make_unique<Reflective>(magenta));

    Object mickey_head{
            std::make_unique<Sphere>(Point{{0, 0, -3}}, 1),
            mickey_material
    };
    scene.addObject(std::move(mickey_head));

    constexpr double ear_radius = .6;
    constexpr double cos_theta = .7;
    double const sin_theta = std::sqrt(1 - cos_theta * cos_theta);

    Object mickey_left_ear{
            std::make_unique<Sphere>(Point{{-(1+ear_radius)*cos_theta, -(1+ear_radius)*sin_theta, -3}}, ear_radius),
            mickey_material
    };
    scene.addObject(std::move(mickey_left_ear));

    Object mickey_right_ear{
            std::make_unique<Sphere>(Point{{(1+ear_radius)*cos_theta, -(1+ear_radius)*sin_theta, -3}}, ear_radius),
            mickey_material
    };
    scene.addObject(std::move(mickey_right_ear));

    Object plane{
            std::make_unique<Plane>(Point{{0, 1, 0}}, Vec{{0, -1, 0}}),
            std::make_shared<FuzzyReflective>(dark_gay, .5)
    };
    scene.addObject(std::move(plane));

    Object ellipsoid{
            std::make_unique<Ellipsoid>(Point{{-12, -3.5, -12}}, 7, 5, 6),
            std::make_shared<Reflective>(yellow)
    };
    scene.addObject(std::move(ellipsoid));

    Object cilinder{
            std::make_unique<Cilinder>(Line{Point{{12, 0, -8}}, Vec{{.5, 1, .1}}}, 2),
            std::make_shared<Reflective>(dark_red)
    };
    scene.addObject(std::move(cilinder));

    return scene;
}

Scene Options::cilinder_scene(Camera camera) {
    using ::ray_tracer::scene::object::Object;
    using ::ray_tracer::scene::object::geometry::Cilinder;
    using ::ray_tracer::scene::object::material::Albedo;
    using ::ray_tracer::scene::object::material::Lambertian;
    using Line = ::ray_tracer::util::StraightLine<double, 3>;

    Scene scene{std::move(camera)};

    Albedo dark_red{.50, .25, .25};

    Object cilinder{
            std::make_unique<Cilinder>(Line{Point{{0, 0, -10}}, Vec::J()}, 3),
            std::make_shared<Lambertian>(dark_red)
    };
    scene.addObject(std::move(cilinder));

    return scene;

}

Scene Options::ellipsoid_scene(Camera camera) {
    using ::ray_tracer::scene::object::Object;
    using ::ray_tracer::scene::object::geometry::Ellipsoid;
    using ::ray_tracer::scene::object::material::Albedo;
    using ::ray_tracer::scene::object::material::Reflective;

    Scene scene(std::move(camera));

    Albedo light_blue{.70, .80, .90};

    Object ellipsoid{
            std::make_unique<Ellipsoid>(Point{{0, 0, -5}}, 3, 1, 1),
            std::make_shared<Reflective>(light_blue)
    };
    scene.addObject(std::move(ellipsoid));

    return scene;
}
