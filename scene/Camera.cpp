//
// Created by luizjardim on 01.09.20.
//

#include "Camera.h"

using ::ray_tracer::scene::Camera;
using Point = ::ray_tracer::scene::Camera::Point;
using Vec = ::ray_tracer::scene::Camera::Vec;

[[nodiscard]] Vec Camera::ray_direction(double u, double v) const {
    return lower_left_corner + u*h_vec + v*v_vec;
}
