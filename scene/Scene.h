//
// Created by luizjardim on 31.08.20.
//

#ifndef RAY_TRACER_SCENE_H
#define RAY_TRACER_SCENE_H

#include "Camera.h"
#include "Object.h"

#include <Color.h>

#include <vector>

namespace ray_tracer::scene {

class Scene {
private:
    using Object = ::ray_tracer::scene::object::Object;
    using Color = ::ray_tracer::util::Color;
public:
    Scene(Camera scene_camera) : camera(std::move(scene_camera)) {}

    Scene& addObject(Object object) {
        objects.push_back(std::move(object));
        return *this;
    }

    [[nodiscard]] Color render(double u, double v, unsigned ray_bounce) const;

private:
    Camera camera;
    std::vector<Object> objects;

    [[nodiscard]] Color render(Ray const&) const;
};

}


#endif //RAY_TRACER_SCENE_H
