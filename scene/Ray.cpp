//
// Created by luizjardim on 01.09.20.
//

#include "Ray.h"
#include "Albedo.h"

using ray_tracer::scene::Ray;
using ::ray_tracer::util::Color;
using ::ray_tracer::scene::object::material::Albedo;

Color Ray::background_color() const {

    double t = 0.5*(direction[1] + 1.0);
    double red = 1.0 - .5*t;
    double green = 1.0 - .3*t;
    double blue = 1.0;

    Albedo background_albedo{red, green, blue};
    return background_albedo.attenuate(color);
}