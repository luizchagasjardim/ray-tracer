#include "gtest/gtest.h"

#include "Ray.h"
#include "object/geometry/Ellipsoid.h"

using ::ray_tracer::scene::Ray;
using ::ray_tracer::scene::object::geometry::Ellipsoid;
using Point = ::ray_tracer::util::Point<double, 3>;
using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::util::Color;

TEST(Ellipsoid_Tests, BunchOfDisorganizedTests) {

    Ellipsoid ellipsoid{Point::ORIGIN(), 3, 1, 2};

    ASSERT_TRUE(ellipsoid.isOn(Point{{3, 0, 0}}));
    ASSERT_TRUE(ellipsoid.isOn(Point{{-3, 0, 0}}));
    ASSERT_TRUE(ellipsoid.isOn(Point{{0, 1, 0}}));
    ASSERT_TRUE(ellipsoid.isOn(Point{{0, -1, 0}}));
    ASSERT_TRUE(ellipsoid.isOn(Point{{0, 0, 2}}));
    ASSERT_TRUE(ellipsoid.isOn(Point{{0, 0, -2}}));

    ASSERT_EQ(*ellipsoid.outward_normal(Point{{3, 0, 0}}), Vec::I());
    ASSERT_EQ(*ellipsoid.outward_normal(Point{{-3, 0, 0}}), -Vec::I());
    ASSERT_EQ(*ellipsoid.outward_normal(Point{{0, 1, 0}}), Vec::J());
    ASSERT_EQ(*ellipsoid.outward_normal(Point{{0, -1, 0}}), -Vec::J());
    ASSERT_EQ(*ellipsoid.outward_normal(Point{{0, 0, 2}}), Vec::K());
    ASSERT_EQ(*ellipsoid.outward_normal(Point{{0, 0, -2}}), -Vec::K());

    ASSERT_DOUBLE_EQ(*ellipsoid.intersection(Ray{Point{{10, 0, 0}}, -Vec::I(), 10, Color::WHITE()}), 7.0);
    ASSERT_DOUBLE_EQ(*ellipsoid.intersection(Ray{Point{{-10, 0, 0}}, Vec::I(), 10, Color::WHITE()}), 7.0);
    ASSERT_DOUBLE_EQ(*ellipsoid.intersection(Ray{Point{{0, 10, 0}}, -Vec::J(), 10, Color::WHITE()}), 9.0);
    ASSERT_DOUBLE_EQ(*ellipsoid.intersection(Ray{Point{{0, -10, 0}}, Vec::J(), 10, Color::WHITE()}), 9.0);
    ASSERT_DOUBLE_EQ(*ellipsoid.intersection(Ray{Point{{0, 0, 10}}, -Vec::K(), 10, Color::WHITE()}), 8.0);
    ASSERT_DOUBLE_EQ(*ellipsoid.intersection(Ray{Point{{0, 0, -10}}, Vec::K(), 10, Color::WHITE()}), 8.0);

}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
