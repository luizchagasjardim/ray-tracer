//
// Created by luizjardim on 30.08.20.
//

#ifndef RAY_TRACER_RAY_H
#define RAY_TRACER_RAY_H

#include "Color.h"
#include "Point.h"
#include "Vec.h"

namespace ray_tracer::scene {

class Ray {
private:
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;
    using Color = ::ray_tracer::util::Color;
public:
    constexpr Ray(Point starting_point, Vec direction_vector, std::size_t bounces, Color ray_color = Color::WHITE())
        : start(std::move(starting_point)), direction(direction_vector.normalized()), remaining_bounces(bounces), color(ray_color) {}

    [[nodiscard]] constexpr Point at(double const& t) const {
        assert(t >= 0);
        return start + t*direction;
    }

    [[nodiscard]] constexpr Point getStart() const {
        return start;
    }

    [[nodiscard]] constexpr Vec getDirection() const {
        return direction;
    }

    [[nodiscard]] constexpr std::size_t getRemainingBounces() const {
        return remaining_bounces;
    }


    [[nodiscard]] constexpr Color getColor() const {
        return color;
    }

    [[nodiscard]] constexpr static std::size_t maxNumberOfPreviousHits() {
        return 5;
    }

    [[nodiscard]] ::ray_tracer::util::Color background_color() const;

private:
    Point start;
    Vec direction;
    std::size_t remaining_bounces;
    Color color;
};

}

#endif //RAY_TRACER_RAY_H
