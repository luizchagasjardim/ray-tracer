//
// Created by luizjardim on 30.08.20.
//

#ifndef RAY_TRACER_CAMERA_H
#define RAY_TRACER_CAMERA_H

#include "../util/Point.h"
#include "../util/Vec.h"

#include <cassert>

namespace ray_tracer::scene {

class Camera {
public:
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;

    class Builder {
    public:
        constexpr Builder() = default;
        constexpr Builder& withSize(double horizontal_size, double vertical_size) {
            h_size = horizontal_size;
            v_size = vertical_size;
            return *this;
        }
        constexpr Builder& at(Point position) {
            pos = std::move(position);
            return *this;
        }
        constexpr Builder& withDirection(Vec direction) {
            dir = std::move(direction);
            return *this;
        }
        constexpr Builder& withUp(Vec up_direction) {
            up = std::move(up_direction);
            return *this;
        }
        constexpr Builder& withFocalLength(double focal_length) {
            focus = std::move(focal_length);
            return *this;
        }
        constexpr Camera build() {
            return Camera{h_size, v_size, pos, dir.normalized(), up.normalized(), focus};
        }
    private:
        double h_size{};
        double v_size{};
        Point pos = Point::ORIGIN();
        Vec dir = Vec::K();
        Vec up = Vec::J();
        double focus = 1;
    };

    [[nodiscard]] constexpr Point position() const {
        return pos;
    }

    [[nodiscard]] Vec ray_direction(double u, double v) const;

private:
    Point pos;
    Vec h_vec;
    Vec v_vec;
    Vec lower_left_corner;

    constexpr Camera(double horizontal_size, double vertical_size, Point position, Vec direction, Vec up_direction, double focal_length)
        : pos{std::move(position)},
        h_vec{horizontal_size*up_direction.cross(direction)},
        v_vec{(vertical_size/horizontal_size)*direction.cross(h_vec)},
        lower_left_corner{- h_vec/2 - v_vec/2 - focal_length*direction}{

    }
};

}

#endif //RAY_TRACER_CAMERA_H
