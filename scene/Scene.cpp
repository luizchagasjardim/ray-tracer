//
// Created by luizjardim on 31.08.20.
//

#include "Scene.h"

#include "Ray.h"

using ::ray_tracer::scene::Ray;
using ::ray_tracer::scene::Scene;
using ::ray_tracer::scene::object::Object;
using ::ray_tracer::util::Color;
using Point = ::ray_tracer::util::Point<double, 3>;
using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::util::double_equal;

Color Scene::render(double u, double v, unsigned ray_bounce) const {
    Ray ray{camera.position(), camera.ray_direction(u, v), ray_bounce};
    return render(ray);
}

Color Scene::render(Ray const &ray) const {

    std::optional<double> intersection = {};
    Object const* intersected_object = nullptr;
    for (auto const& object : objects) {
        std::optional<double> object_intersection = object.intersection(ray);
        if (!object_intersection || double_equal(*object_intersection, 0))
            continue;
        if (!intersection || *intersection > *object_intersection) {
            intersection = object_intersection;
            intersected_object = &object;
        }
    }

    if (!intersection)
        return ray.background_color();

    Point intersection_point = ray.at(*intersection);
    assert(intersected_object->isOn(intersection_point));
    Ray reflected_ray{intersected_object->reflect(ray, intersection_point)};

    if (reflected_ray.getRemainingBounces() == 0)
        return Color::BLACK();

    return render(reflected_ray);
}
