//
// Created by luizjardim on 05.09.20.
//

#ifndef RAY_TRACER_OBJECT_H
#define RAY_TRACER_OBJECT_H

#include "Geometry.h"
#include "Material.h"

#include <memory>

namespace ray_tracer::scene::object {
    class Object {
        using Geometry_ptr = std::unique_ptr<::ray_tracer::scene::object::geometry::Geometry>;
        using Material_ptr = std::shared_ptr<::ray_tracer::scene::object::material::Material>;
        using Point = ::ray_tracer::util::Point<double, 3>;
        using Vec = ::ray_tracer::util::Vec<double, 3>;
    public:
        explicit Object(Geometry_ptr&& geometry_ptr, Material_ptr material_ptr) : geometry(std::move(geometry_ptr)), material{std::move(material_ptr)} {}

        [[nodiscard]] std::optional<double> intersection(Ray const& ray) const {
            return geometry->intersection(ray);
        }
        [[nodiscard]] std::optional<Vec> outward_normal(Point const& p) const {
            return geometry->outward_normal(p);
        }
        [[nodiscard]] bool isOn(Point const& p) const {
            return geometry->isOn(p);
        }

        [[nodiscard]] Ray reflect(Ray const& incident_ray, Point const& point_of_intersection) const;

    private:
        Geometry_ptr geometry;
        Material_ptr material;
    };
}

#endif //RAY_TRACER_OBJECT_H
