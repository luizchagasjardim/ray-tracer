//
// Created by luizjardim on 15.09.20.
//

#include "Refractive.h"
#include "Vec.h"

using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::scene::object::material::Refractive;
using ::ray_tracer::util::Color;

namespace {
double sin_from_cos(double cos) {
    return std::sqrt(1 - cos*cos);
}
}

Vec Refractive::get_reflected_direction(Vec const& incident_direction, Vec const& normal) {
    double cos_incident_angle = incident_direction.dot(normal); //asumes both are normalized
    double sin_incident_angle = sin_from_cos(cos_incident_angle);

    bool ray_is_coming_from_inside = cos_incident_angle > 0;
    double refraction_index = ray_is_coming_from_inside ? 1 / index : index;

    double sin_refracted_angle = sin_incident_angle / refraction_index;

    if (sin_refracted_angle > 1 || random.number() < reflection_probability(cos_incident_angle))
        return reflection->get_reflected_direction(incident_direction, normal);

    double cos_refracted_angle = sin_from_cos(sin_refracted_angle);
    return incident_direction / refraction_index + (cos_incident_angle - cos_refracted_angle) * normal;
}

Color Refractive::get_reflected_color(Color const& incident_color) {
    return albedo.attenuate(incident_color);
}

double Refractive::reflection_probability(double cos_incident_angle) const {
    auto aux = (1-index) / (1+index);
    aux = aux*aux;
    return aux + (1-aux)*pow((1 + cos_incident_angle),5);
}