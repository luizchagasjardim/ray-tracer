//
// Created by luizjardim on 15.09.20.
//

#ifndef RAY_TRACER_REFRACTIVE_H
#define RAY_TRACER_REFRACTIVE_H

#include "Material.h"
#include "Albedo.h"

#include <memory>
#include <RandomGenerator.h>

namespace ray_tracer::scene::object::material {

    class Refractive final : public Material {
        using Vec = ::ray_tracer::util::Vec<double, 3>;
        using Color = ::ray_tracer::util::Color;
        using Random = ::ray_tracer::util::RandomGenerator<double, 3>;
    public:
        explicit Refractive(Albedo material_color, double relative_refraction_index, std::unique_ptr<Material>&& reflective_behaviour)
            : albedo{std::move(material_color)},
            index{relative_refraction_index},
            reflection{std::move(reflective_behaviour)} {}
        [[nodiscard]] Vec get_reflected_direction(Vec const& incident_direction, Vec const& normal) override;
        [[nodiscard]] Color get_reflected_color(Color const& incident_color) override;
    private:
        Albedo albedo;
        double index = 1;
        std::unique_ptr<Material> reflection;
        Random random;

        [[nodiscard]] double reflection_probability(double cos_incident_angle) const;
    };

}


#endif //RAY_TRACER_REFRACTIVE_H
