//
// Created by luizjardim on 07.09.20.
//

#ifndef RAY_TRACER_REFLECTIVE_H
#define RAY_TRACER_REFLECTIVE_H


#include "Material.h"
#include "Albedo.h"

namespace ray_tracer::scene::object::material {

class Reflective final : public Material {
    using Vec = ::ray_tracer::util::Vec<double, 3>;
    using Color = ::ray_tracer::util::Color;
public:
    explicit Reflective(Albedo material_color) : albedo(std::move(material_color)) {}
    [[nodiscard]] Vec get_reflected_direction(Vec const& incident_direction, Vec const& normal) override;
    [[nodiscard]] Color get_reflected_color(Color const& incident_color) override;
private:
    Albedo albedo;
};

}


#endif //RAY_TRACER_REFLECTIVE_H
