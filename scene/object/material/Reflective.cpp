//
// Created by luizjardim on 07.09.20.
//

#include "Reflective.h"
#include "Vec.h"

using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::scene::object::material::Reflective;
using ::ray_tracer::util::Color;

Vec Reflective::get_reflected_direction(Vec const& incident_direction, Vec const& normal) {
    Vec projection_on_normal = incident_direction.dot(normal) * normal;
    return incident_direction - 2 * projection_on_normal;
}

Color Reflective::get_reflected_color(Color const& incident_color) {
    return albedo.attenuate(incident_color);
}