//
// Created by luizjardim on 07.09.20.
//

#ifndef RAY_TRACER_ALBEDO_H
#define RAY_TRACER_ALBEDO_H

#include "DoubleUtil.h"
#include "Color.h"

namespace ray_tracer::scene::object::material {

class Albedo {
    using Color = ::ray_tracer::util::Color;
public:
    constexpr Albedo(double red_attenuation, double green_attenuation, double blue_attenuation)
        : r{clamp(red_attenuation)}, g{clamp(green_attenuation)}, b{clamp(blue_attenuation)} {}
    Color attenuate(Color const& color) {
        using byte = Color::byte;
        byte red = r * color.getRed();
        byte green = g * color.getGreen();
        byte blue = b * color.getBlue();
        return Color::fromRgbValues(red, green, blue);
    }
private:
    double r;
    double g;
    double b;

    constexpr static double clamp(double value) {
        return ::ray_tracer::util::clamp<double>(0.0, value, 1.0);
    }
};

}

#endif //RAY_TRACER_ALBEDO_H
