//
// Created by luizjardim on 06.09.20.
//

#include "Lambertian.h"

#include "Vec.h"

using ::ray_tracer::scene::object::material::Lambertian;
using Vec = ::ray_tracer::util::Vec<double, 3>;
using Point = ::ray_tracer::util::Point<double, 3>;
using ::ray_tracer::util::Color;

Vec Lambertian::get_reflected_direction(Vec const&, Vec const& normal) {
    Vec direction = random.unitVector() + normal;
    return direction.normalized();
}

Color Lambertian::get_reflected_color(Color const& incident_color) {
    return albedo.attenuate(incident_color);
}