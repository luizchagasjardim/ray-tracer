//
// Created by luizjardim on 13.09.20.
//

#include "FuzzyReflective.h"
#include "Reflective.h"
#include "Vec.h"

using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::scene::object::material::FuzzyReflective;
using ::ray_tracer::util::Color;

Vec FuzzyReflective::get_reflected_direction(Vec const& incident_direction, Vec const& normal) {
    return reflective.get_reflected_direction(incident_direction, normal) + fuzzyness * randomGenerator.vectorInsideUnitSphere();
}

Color FuzzyReflective::get_reflected_color(Color const& incident_color) {
    return reflective.get_reflected_color(incident_color);
}