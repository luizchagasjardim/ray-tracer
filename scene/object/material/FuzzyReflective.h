//
// Created by luizjardim on 13.09.20.
//

#ifndef RAY_TRACER_FUZZYREFLECTIVE_H
#define RAY_TRACER_FUZZYREFLECTIVE_H

#include "Material.h"
#include "Reflective.h"
#include "RandomGenerator.h"

namespace ray_tracer::scene::object::material {

    class FuzzyReflective final : public Material {
        using Vec = ::ray_tracer::util::Vec<double, 3>;
        using Color = ::ray_tracer::util::Color;
        using RandomGenerator = ::ray_tracer::util::RandomGenerator<double, 3>;
    public:
        explicit FuzzyReflective(Albedo material_color, double reflection_fuzzyness)
        : reflective{std::move(material_color)}, fuzzyness{::ray_tracer::util::clamp(0.0, reflection_fuzzyness, 1.0)} {}

        [[nodiscard]] Vec get_reflected_direction(Vec const& incident_direction, Vec const& normal) override;
        [[nodiscard]] Color get_reflected_color(Color const& incident_color) override;
    private:
        Reflective reflective;
        double fuzzyness;
        RandomGenerator randomGenerator;
    };

}


#endif //RAY_TRACER_FUZZYREFLECTIVE_H
