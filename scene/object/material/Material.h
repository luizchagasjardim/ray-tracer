//
// Created by luizjardim on 04.09.20.
//

#ifndef RAY_TRACER_MATERIAL_H
#define RAY_TRACER_MATERIAL_H

#include <array>

namespace ray_tracer::util {
template<typename T, std::size_t size>
class Vec;
class Color;
}

namespace ray_tracer::scene::object::material {

class Material {
    using Vec = ::ray_tracer::util::Vec<double, 3>;
    using Color = ::ray_tracer::util::Color;
public:
    [[nodiscard]] virtual Vec get_reflected_direction(Vec const& incident_direction, Vec const& normal) = 0;
    [[nodiscard]] virtual Color get_reflected_color(Color const& incident_color) = 0;
    virtual ~Material() = default;
};

}

#endif //RAY_TRACER_MATERIAL_H
