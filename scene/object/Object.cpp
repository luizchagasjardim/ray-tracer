//
// Created by luizjardim on 06.09.20.
//

#include "Object.h"
#include "Ray.h"
#include "Point.h"
#include "Vec.h"

using ::ray_tracer::scene::object::Object;
using ::ray_tracer::scene::Ray;
using Point = ::ray_tracer::util::Point<double, 3>;
using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::util::Color;

[[nodiscard]] Ray Object::reflect(Ray const& incident_ray, Point const& point_of_intersection) const {
    assert(geometry->isOn(point_of_intersection));
    Vec normal = *(geometry->outward_normal(point_of_intersection));
    Vec reflected_direction = material->get_reflected_direction(incident_ray.getDirection(), normal);
    Color reflect_ray_color = material->get_reflected_color(incident_ray.getColor());
    return Ray{
        point_of_intersection,
        reflected_direction,
        incident_ray.getRemainingBounces() - 1,
        reflect_ray_color
    };
}
