//
// Created by luizjardim on 31.08.20.
//

#ifndef RAY_TRACER_GEOMETRY_H
#define RAY_TRACER_GEOMETRY_H

#include "Vec.h"

#include <optional>

namespace ray_tracer {
namespace util {
    template<typename T, std::size_t size>
    class Point;
}
namespace scene {
    class Ray;
}
}

namespace ray_tracer::scene::object::geometry {

class Geometry {
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;
public:
    [[nodiscard]] virtual std::optional<double> intersection(Ray const&) const = 0;
    [[nodiscard]] virtual std::optional<Vec> outward_normal(Point const&) const = 0;
    [[nodiscard]] virtual bool isOn(Point const&) const = 0;
    virtual ~Geometry() = default;
};

}

#endif //RAY_TRACER_GEOMETRY_H
