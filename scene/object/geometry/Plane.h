//
// Created by luizjardim on 05.09.20.
//

#ifndef RAY_TRACER_PLANE_H
#define RAY_TRACER_PLANE_H

#include "Geometry.h"

#include <Point.h>
#include <Vec.h>
#include <DoubleUtil.h>

namespace ray_tracer::scene::object::geometry {

class Plane final : public Geometry {
private:
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;
    using Ray = ::ray_tracer::scene::Ray;
public:
    constexpr Plane(Point point_on_plane, Vec normal_vector) : point{std::move(point_on_plane)}, normal{normal_vector.normalized()} {}

    [[nodiscard]] bool isOn(Point const& p) const override {
        Vec vector_supposedly_on_plane{point, p};
        double dot_product = vector_supposedly_on_plane.dot(normal);
        return ::ray_tracer::util::double_equal(dot_product, 0);
    }

    [[nodiscard]] std::optional<Vec> outward_normal(Point const& point) const override {
        if (!isOn(point))
            return {};
        return normal;
    }

    [[nodiscard]] std::optional<double> intersection(Ray const& ray) const override;

private:

    [[nodiscard]] bool isParallel(Vec const& v) const {
        return ::ray_tracer::util::double_equal(normal.dot(v), 0);
    }

    Point point;
    Vec normal;
};

}

#endif //RAY_TRACER_PLANE_H
