//
// Created by luizjardim on 31.08.20.
//

#ifndef RAY_TRACER_SPHERE_H
#define RAY_TRACER_SPHERE_H

#include "Geometry.h"

#include <Point.h>
#include <Vec.h>
#include <DoubleUtil.h>

namespace ray_tracer::scene::object::geometry {

class Sphere final : public Geometry {
private:
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;
    using Ray = ::ray_tracer::scene::Ray;
public:
    constexpr Sphere(Point sphere_center, double sphere_radius) : center{std::move(sphere_center)}, radius{sphere_radius} {}

    [[nodiscard]] bool isOn(Point const& p) const override {
        return ::ray_tracer::util::double_equal(p.distance_squared_to(center), radius*radius);
    }

    [[nodiscard]] std::optional<Vec> outward_normal(Point const& point) const override {
        if (!isOn(point))
            return {};
        return Vec{center, point}.normalized();
    }

    [[nodiscard]] std::optional<double> intersection(Ray const& r) const override;

private:
    Point center;
    double radius;
};

}

#endif //RAY_TRACER_SPHERE_H
