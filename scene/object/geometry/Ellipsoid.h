//
// Created by luizjardim on 12.09.20.
//

#ifndef RAY_TRACER_ELLIPSOID_H
#define RAY_TRACER_ELLIPSOID_H

#include "Geometry.h"

namespace ray_tracer::scene::object::geometry {

class Ellipsoid : public Geometry {
    using Point = ray_tracer::util::Point<double, 3>;
    using Vec = ray_tracer::util::Vec<double, 3>;
public:
    Ellipsoid(Point ellipsoid_center, double x_axis_length, double y_axis_length, double z_axis_length)
        : center{std::move(ellipsoid_center)}, a{x_axis_length}, b{y_axis_length}, c{z_axis_length} {}

    [[nodiscard]] bool isOn(Point const& p) const override;

    [[nodiscard]] std::optional<Vec> outward_normal(Point const& p) const override;

    [[nodiscard]] std::optional<double> intersection(Ray const& r) const override;
private:
    Point center;
    double a;
    double b;
    double c;

    template<typename T>
    constexpr T squish(T const& t) const {
        return T{{t[0]/a, t[1]/b, t[2]/c}};
    }
};

}

#endif //RAY_TRACER_ELLIPSOID_H
