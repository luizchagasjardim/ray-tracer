//
// Created by luizjardim on 12.09.20.
//

#include "Ellipsoid.h"

#include "DoubleUtil.h"
#include "Ray.h"
#include "Sphere.h"

using ::ray_tracer::scene::object::geometry::Ellipsoid;
using Point = ::ray_tracer::util::Point<double, 3>;
using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::scene::Ray;

[[nodiscard]] bool Ellipsoid::isOn(Point const& p) const {
    Vec from_center = Vec{center, p};
    //We will stretch/squish everything in the 3d space so that the ellipsoid becomes a sphere with radius 1
    Vec squished = squish(from_center);
    return ::ray_tracer::util::double_equal(squished.lenght_squared(), 1);
}

[[nodiscard]] std::optional<Vec> Ellipsoid::outward_normal(Point const& p) const {
    if (!isOn(p))
        return {};
    //The normal has to be squished twice to compensate for the points being squished
    return squish(squish(Vec{center, p})).normalized();
}

[[nodiscard]] std::optional<double> Ellipsoid::intersection(Ray const& ray) const {
    //We will stretch/squish everything in the 3d space so that the ellipsoid becomes a sphere with radius 1
    Vec const squished_direction = squish(ray.getDirection());
    double time_dilation = squished_direction.lenght();
    Ray new_ray{squish(ray.getStart()), std::move(squished_direction), ray.getRemainingBounces(), ray.getColor()};
    Point new_center = squish(center);
    double new_radius = 1;
    std::optional<double> intercection = Sphere{new_center, new_radius}.intersection(new_ray);
    if (!intercection)
        return {};
    return *intercection / time_dilation;
}