//
// Created by luizjardim on 05.09.20.
//

#ifndef RAY_TRACER_CILINDER_H
#define RAY_TRACER_CILINDER_H

#include "Geometry.h"

#include <Point.h>
#include <Vec.h>
#include <DoubleUtil.h>
#include <StraightLine.h>

namespace ray_tracer::scene::object::geometry {

class Cilinder final : public Geometry {
private:
    using Point = ::ray_tracer::util::Point<double, 3>;
    using Vec = ::ray_tracer::util::Vec<double, 3>;
    using Line = ::ray_tracer::util::StraightLine<double, 3>;
    using Ray = ::ray_tracer::scene::Ray;
public:
    constexpr Cilinder(Line cilinder_center, double cilinder_radius)
        : center{std::move(cilinder_center)}, radius{std::move(cilinder_radius)} {}

    [[nodiscard]] bool isOn(Point const& p) const override {
        return ::ray_tracer::util::double_equal(center.distance_squared_to(p), radius*radius);
    }

    [[nodiscard]] std::optional<Vec> outward_normal(Point const& point) const override {
        if (!isOn(point))
            return {};
        return Vec{center.closest_to(point), point}.normalized();
    }

    [[nodiscard]] std::optional<double> intersection(Ray const& ray) const override;

private:
    Line center;
    double radius;
};

}

#endif //RAY_TRACER_CILINDER_H
