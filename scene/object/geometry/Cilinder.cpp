//
// Created by luizjardim on 05.09.20.
//

#include "Cilinder.h"
#include "Ray.h"
#include "SecondDegreePolynomial.h"

using ::ray_tracer::scene::object::geometry::Cilinder;
using ::ray_tracer::scene::Ray;
using Vec = ::ray_tracer::util::Vec<double, 3>;
using ::ray_tracer::util::SecondDegreePolynomial;

[[nodiscard]] std::optional<double> Cilinder::intersection(Ray const& ray) const {
    // cilinder equation: |(p-c1) cross d1 | = r aka distance from the point to the center line = radius
    // ray equation: p = c2 + td2
    // substitute one into the other
    // | (c2 - c1 + td2) cross d1 | = r
    // call aux = (c2 - c1) cross d1 and n = d2 cross d1
    // | aux + t n | = r
    // remember that | v |² = v . v
    // | aux |² + t * 2 aux . n + t² = r²
    // |n|² t² + (2 aux . n) * t + ( | aux |² - r² ) = 0

    Vec d1 = center.getDirection().normalized();
    Vec aux = Vec{center.getPoint(), ray.getStart()}.cross(d1);
    Vec normal = ray.getDirection().cross(d1);

    double a = normal.lenght_squared();
    double b = 2.0 * aux.dot(normal);
    double c = aux.lenght_squared() - radius*radius;

    return SecondDegreePolynomial{a, b, c}.first_positive_root();
}