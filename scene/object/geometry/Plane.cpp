//
// Created by luizjardim on 05.09.20.
//

#include "Plane.h"
#include "Ray.h"

using ::ray_tracer::scene::object::geometry::Plane;
using ::ray_tracer::scene::Ray;


std::optional<double> Plane::intersection(Ray const& ray) const {
    Vec ray_direction = ray.getDirection();
    Vec aux = Vec{point, ray.getStart()};

    //There are four possibilities:
    //1. plane contains ray => intersects at start
    //2. ray is parallel but not on plane
    //3. ray intersects plane on exactly one point
    //4. ray doesn't intersect because intersection would be negative

    if (isParallel(ray_direction)) {
        if (isParallel(aux))
            return 0;
        return {};
    }

    double intersection = - normal.dot(aux) / normal.dot(ray_direction);

    if (intersection >= 0)
        return intersection;
    return {};
}
