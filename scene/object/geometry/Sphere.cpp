//
// Created by luizjardim on 01.09.20.
//

#include "Sphere.h"

#include "Point.h"
#include "Ray.h"
#include "SecondDegreePolynomial.h"

using Point = ::ray_tracer::util::Point<double, 3>;
using ::ray_tracer::scene::object::geometry::Sphere;
using ::ray_tracer::scene::Ray;
using ::ray_tracer::util::SecondDegreePolynomial;

std::optional<double> Sphere::intersection(Ray const& r) const {
    Vec direction = r.getDirection();
    Vec oc = Vec{center, r.getStart()};

    double a = direction.lenght_squared();
    double b = 2.0 * oc.dot(direction);
    double c = oc.lenght_squared() - radius*radius;

    return SecondDegreePolynomial{a, b, c}.first_positive_root();
}
