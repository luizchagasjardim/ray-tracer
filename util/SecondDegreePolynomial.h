//
// Created by luizjardim on 04.09.20.
//

#ifndef RAY_TRACER_SECONDDEGREEPOLYNOMIAL_H
#define RAY_TRACER_SECONDDEGREEPOLYNOMIAL_H

#include <cmath>

namespace ray_tracer::util {

template<typename T>
class SecondDegreePolynomial {
public:
    constexpr SecondDegreePolynomial(T x_squared_coefficient, T x_coefficient, T independent_term)
        : a{x_squared_coefficient}, b{x_coefficient}, c{independent_term} {
        assert(a != 0);
    }

    [[nodiscard]] constexpr T at(T t) const {
        return a*t*t + b*t + c;
    }

    [[nodiscard]] constexpr unsigned numberOfRoots() const {
        T discriminant = b*b - 4*a*c;
        if (discriminant > 0)
            return 2;
        if (discriminant == 0)
            return 1;
        return 0;
    }

    [[nodiscard]] constexpr T first_root() const {
        assert(numberOfRoots() > 0);
        T root = - b - std::sqrt(b*b - 4*a*c);
        root /= 2*a;
        return root;
    }

    [[nodiscard]] constexpr T second_root() const {
        assert(numberOfRoots() > 0);
        T root = - b + std::sqrt(b*b - 4*a*c);
        root /= 2*a;
        return root;
    }

    [[nodiscard]] constexpr std::optional<T> first_positive_root() const {
        unsigned n = numberOfRoots();
        if (n == 0)
            return {};

        T root1 = first_root();
        if (root1 > 0)
            return root1;
        if (n == 1)
            return {};

        T root2 = second_root();
        if (root2 > 0)
            return root2;
        return {};
    }

private:
    T a{};
    T b{};
    T c{};
};

}

#endif //RAY_TRACER_SECONDDEGREEPOLYNOMIAL_H
