//
// Created by luizjardim on 06.09.20.
//

#ifndef RAY_TRACER_COLORAVERAGE_H
#define RAY_TRACER_COLORAVERAGE_H

#include "Color.h"

namespace ray_tracer::util {

class ColorAverage {
public:

    constexpr ColorAverage() = default;

    ColorAverage& addColor(Color const& color) {
        red += color.getRed();
        green += color.getGreen();
        blue += color.getBlue();
        n += 1;
        return *this;
    }

    [[nodiscard]] constexpr ColorAverage addColor(Color const& color) const {
        return ColorAverage(red + color.getRed(), green + color.getGreen(), blue + color.getBlue(), n+1);
    }

    [[nodiscard]] constexpr Color average() const {
        using byte = Color::byte;
        byte r = clamp<long long unsigned>(0llu, red/n, 255llu);
        byte g = clamp<long long unsigned>(0llu, green/n, 255llu);
        byte b = clamp<long long unsigned>(0llu, blue/n, 255llu);
        return Color::fromRgbValues(r, g, b);
    }

private:
    long long unsigned red{};
    long long unsigned green{};
    long long unsigned blue{};
    long long unsigned n{};

    constexpr ColorAverage(long long unsigned r, long long unsigned g, long long unsigned b, long long unsigned count)
        : red(r), green(g), blue(b), n(count) {}
};

}

#endif //RAY_TRACER_COLORAVERAGE_H
