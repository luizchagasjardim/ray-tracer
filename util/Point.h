//
// Created by luizjardim on 29.08.20.
//

#ifndef RAY_TRACER_POINT_H
#define RAY_TRACER_POINT_H

#include <array>
#include <cmath>
#include <iostream>
#include <numeric>

namespace ray_tracer::util {

template<typename T, std::size_t size>
class Point {
public:
    explicit constexpr Point(std::array<T, size> values) : coordinates(std::move(values)) {}

    //constants
    [[nodiscard]] constexpr static Point ORIGIN() {
        return Point{};
    }

    [[nodiscard]] constexpr bool operator==(Point const& other) const {
        for (std::size_t i = 0; i < size; ++i)
            if(coordinates[i] != other.coordinates[i])
                return false;
        return true;
    }

    [[nodiscard]] constexpr T operator[](std::size_t i) const {
        return coordinates[i];
    }

    [[nodiscard]] constexpr T& operator[](std::size_t i) {
        return coordinates[i];
    }

    inline friend std::ostream& operator<<(std::ostream& os, Point const& p) {
        os << '(';
        constexpr std::size_t last = size-1;
        for (std::size_t i = 0; i < last; ++i)
            os << p[i] << ", ";
        os << p[last];
        os << ')';
        return os;
    }

    constexpr T distance_squared_to(Point const& other) const {
        T distance_squared = 0;
        for (std::size_t i = 0; i < size; ++i) {
            T difference = coordinates[i] - other[i];
            distance_squared += difference * difference;
        }
        return distance_squared;
    }

    constexpr T distance_to(Point const& other) const {
        return std::sqrt(distance_squared_to(other));
    }

private:
    std::array<T, size> coordinates;

    constexpr Point() = default;
};

}

#endif //RAY_TRACER_POINT_H
