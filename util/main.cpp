#include <gtest/gtest.h>

#include "Color.h"
#include "Point.h"
#include "SecondDegreePolynomial.h"
#include "Vec.h"

using namespace ::ray_tracer::util;
using Point3f = Point<double, 3>; //TODO rename
using Vec3f = Vec<double, 3>; //TODO rename

TEST(UnorganizedTests, TODO) {
    Point3f p = Point3f::ORIGIN();
    p[2] = 1;
    EXPECT_EQ(p, Point3f({0, 0, 1}));
    constexpr Point3f constexpr_point{{3, 3, 3}};
    constexpr float y = constexpr_point[1];
    EXPECT_EQ(y, 3);
    EXPECT_EQ(constexpr_point[1], 3);
    EXPECT_EQ((Vec<float, 0>::ZERO()), (Vec<float, 0>{{}}));
    EXPECT_EQ((Vec<float, 1>::I()), (Vec<float, 1>::I(0)));
    EXPECT_EQ(Vec3f::J(), Vec3f::I(1));
    EXPECT_EQ(Vec3f::K(), Vec3f::I(2));
    Vec3f v4{Point3f::ORIGIN(), Point3f{{0, 4, 1}}};
    EXPECT_EQ(v4, Vec3f({0, 4, 1}));
    EXPECT_EQ(Color{}, Color::BLACK());
    EXPECT_EQ(Color::fromRgbValues(255, 0, 255).getRed(), 255);
    EXPECT_EQ(Vec3f({3, -1, 2}), 3*Vec3f::I() - Vec3f::J() + 2*Vec3f::K());
    EXPECT_DOUBLE_EQ(2.0 * Vec3f({2, 0, 4}).dot(Vec3f({0, 0, -1})), -8);
//    constexpr double dot = Vec3f({2, 0, 4}).dot(Vec3f({-0.17, -0.09, -0.98}));
}

TEST(Vec3fConstructor, InitializerList) {
    Vec3f v{{3, 2, 4}};
    EXPECT_EQ(v[0], 3);
    EXPECT_EQ(v[1], 2);
    EXPECT_EQ(v[2], 4);
}

//TODO: turn into parametrized tests
class Vec3fOperations : public ::testing::Test {
protected:
    Vec3f v1{{1, 5, 2}};
    Vec3f v2{{-3, 0, 1}};
};

TEST_F(Vec3fOperations, Addition) {
    EXPECT_EQ(v1 + v2, Vec3f({-2, 5, 3}));
}

TEST_F(Vec3fOperations, Subtraction) {
    EXPECT_EQ(v1 - v2, Vec3f({4, 5, 1}));
}

TEST_F(Vec3fOperations, Dot) {
    EXPECT_FLOAT_EQ(v1.dot(v2), -3+0+2);
}

TEST_F(Vec3fOperations, Cross) {
    EXPECT_EQ(v1.cross(v2), Vec3f({5-0,-6-1,0+15}));
    EXPECT_EQ(Vec3f::I().cross(Vec3f::J()), Vec3f::K());
    EXPECT_EQ(Vec3f::J().cross(Vec3f::K()), Vec3f::I());
    EXPECT_EQ(Vec3f::K().cross(Vec3f::I()), Vec3f::J());
    EXPECT_EQ(Vec3f::I().cross(Vec3f::K()), -Vec3f::J());
    EXPECT_EQ(Vec3f::K().cross(Vec3f::J()), -Vec3f::I());
    EXPECT_EQ(Vec3f::J().cross(Vec3f::I()), -Vec3f::K());
}

TEST_F(Vec3fOperations, Length) {
    EXPECT_FLOAT_EQ(v1.lenght_squared(), 1*1 + 5*5 + 2*2);
    EXPECT_FLOAT_EQ(v1.lenght(), 5.47722557505f);

    EXPECT_FLOAT_EQ(v2.lenght_squared(), 3*3 + 1*1);
    EXPECT_FLOAT_EQ(v2.lenght(), 3.16227766017f);
}

class SecondDegreePolynomialTests : public ::testing::Test {
protected:
    SecondDegreePolynomial<float> p1{1, 0, 0};
    SecondDegreePolynomial<float> p2{1, 0, 1};
    SecondDegreePolynomial<float> p3{1, 0, -1};
};

TEST_F(SecondDegreePolynomialTests, NumberOfRoots) {
    EXPECT_EQ(p1.numberOfRoots(), 1u);
    EXPECT_EQ(p2.numberOfRoots(), 0u);
    EXPECT_EQ(p3.numberOfRoots(), 2u);
}

TEST_F(SecondDegreePolynomialTests, FirstRoot) {
    EXPECT_FLOAT_EQ(p1.first_root(), 0.0f);
    EXPECT_FLOAT_EQ(p3.first_root(), -1.0f);
}

TEST_F(SecondDegreePolynomialTests, SecondRoot) {
    EXPECT_FLOAT_EQ(p3.second_root(), 1.0f);
}

TEST(RandomGeneratorTests, BunchOfDisorganizedTests) {
    EXPECT_NEAR(std::sin(PI()), 0.0, 0.0001);
    EXPECT_NEAR(std::sin(PI()/2), 1.0, 0.0001);
    EXPECT_NEAR(std::sin(PI()/4), std::sqrt(2)/2, 0.0001);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
