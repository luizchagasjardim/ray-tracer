//
// Created by luizjardim on 29.08.20.
//

#ifndef RAY_TRACER_COLOR_H
#define RAY_TRACER_COLOR_H

#include "DoubleUtil.h"

#include <ostream>

namespace ray_tracer::util {

class Color {
public:
    using byte = unsigned char;

    constexpr Color() = default;

    [[nodiscard]] constexpr static Color fromRgbValues(byte red, byte green, byte blue) {
        return Color(red, green, blue);
    }

    [[nodiscard]] constexpr static Color fromHsvValues(byte hue, byte saturation, byte value) {
        (void) hue;
        (void) saturation;
        (void) value;
        return BLACK();
    }

    [[nodiscard]] constexpr byte getRed() const {
        return r;
    }

    [[nodiscard]] constexpr byte getGreen() const {
        return g;
    }

    [[nodiscard]] constexpr byte getBlue() const {
        return b;
    }

    //TODO getHue, getSaturation, getValue

    //constants
    [[nodiscard]] constexpr static Color BLACK() {
        return Color(0, 0, 0);
    }
    [[nodiscard]] constexpr static Color WHITE() {
        return Color(255, 255, 255);
    }
    //TODO: more constants

    [[nodiscard]] constexpr Color gammaCorrection() const {
        byte red = std::sqrt(256*r);
        byte green = std::sqrt(256*g);
        byte blue = std::sqrt(256*b);
        return Color(red, green, blue);
    }

    [[nodiscard]] constexpr Color gammaCorrection(double gamma) const {
        double inverse_gamma = 1/gamma;
        double complement = 1 - inverse_gamma;
        byte red = std::pow(r, inverse_gamma) * std::pow(256, complement);
        byte green = std::pow(g, inverse_gamma) * std::pow(256, complement);
        byte blue = std::pow(b, inverse_gamma) * std::pow(256, complement);
        return Color(red, green, blue);
    }

    [[nodiscard]] constexpr static Color gradient(Color const& from, Color const& to, double t) {
        clamp(0.0, t, 1.0);
        //TODO use color operators when available
        byte r = t*from.r + (1-t)*to.r;
        byte g = t*from.g + (1-t)*to.g;
        byte b = t*from.b + (1-t)*to.b;
        return Color(r, g, b);
    }

    inline friend std::ostream& operator<<(std::ostream& os, Color const& c) {
        return os << +c.r << ' ' << +c.g << ' ' << +c.b;
    }

    [[nodiscard]] constexpr Color operator*(double factor) const {
        double red = clamp(0.0, factor * r, 255.999);
        double green = clamp(0.0, factor * g, 255.999);
        double blue = clamp(0.0, factor * b, 255.999);

        return Color{static_cast<byte>(red), static_cast<byte>(green), static_cast<byte>(blue)};
    }

    [[nodiscard]] inline friend constexpr Color operator*(double factor, Color const& color) {
        return color * factor;
    }
    [[nodiscard]] constexpr bool operator==(Color const& other) const {
        return r == other.r && g == other.g && b == other.b;
    }
    //TODO: arithmetic operators if necessary

private:
    byte r{};
    byte g{};
    byte b{};

    constexpr Color(byte red, byte green, byte blue) : r(red), g(green), b(blue) {}
};
}

#endif //RAY_TRACER_COLOR_H
