//
// Created by luizjardim on 29.08.20.
//

#ifndef RAY_TRACER_VEC_H
#define RAY_TRACER_VEC_H

#include "Point.h"

#include <array>
#include <cassert>
#include <cmath>
#include <functional>
#include <numeric>

namespace ray_tracer::util {

template<typename T, std::size_t size>
class Vec {
public:
    using Point_t = Point<T, size>;

    constexpr explicit Vec(std::array<T, size> values) : coordinates(std::move(values)) {}
    constexpr explicit Vec(Point_t const& from, Point_t const& to) {
        for (std::size_t i = 0; i < size; ++i)
            coordinates[i] = to[i] - from[i];
    }

    bool operator==(Vec const& other) const {
        for (std::size_t i = 0; i < size; ++i)
            if(coordinates[i] != other.coordinates[i])
                return false;
        return true;
    }

    [[nodiscard]] constexpr T operator[](std::size_t i) const {
        return coordinates[i];
    }

    [[nodiscard]] constexpr T& operator[](std::size_t i) {
        return coordinates[i];
    }

    //constants
    [[nodiscard]] constexpr static Vec ZERO() {
        return Vec{};
    }
    [[nodiscard]] constexpr static Vec I(std::size_t canonical_direction) {
        assert(canonical_direction < size);
        Vec i{};
        i[canonical_direction] = 1;
        return i;
    }
    template<typename = void> requires (size >= 1)
    [[nodiscard]] constexpr static Vec I() {
        return I(0);
    }
    template<typename = void> requires (size >= 2)
    [[nodiscard]] constexpr static Vec J() {
        return I(1);
    }
    template<typename = void> requires (size >= 3)
    [[nodiscard]] constexpr static Vec K() {
        return I(2);
    }

    //unary operations
    [[nodiscard]] constexpr T lenght_squared() const {
        T result = 0;
        for (T c : coordinates)
            result += c*c;
        return result;
    }
    [[nodiscard]] constexpr T lenght() const {
        return std::sqrt(lenght_squared());
    }
    [[nodiscard]] constexpr Vec normalized() const {
        return operator/(lenght());
    }
    [[nodiscard]] constexpr Vec operator-() const {
        return coordinate_wise_operation([](T t) -> T {return -t;});
    }
    //TODO

    //binary operations
    [[nodiscard]] constexpr Vec operator+(Vec const& other) const {
        return coordinate_wise_operation(other, [](T t1, T t2) -> T {return t1 + t2;});
    }
    [[nodiscard]] constexpr Vec operator-(Vec const& other) const {
        return coordinate_wise_operation(other, [](T t1, T t2) -> T {return t1 - t2;});
    }
    [[nodiscard]] constexpr Vec operator*(T operand) const {
        return coordinate_wise_operation([operand](T coordinate) -> T {return operand * coordinate;});
    }
    [[nodiscard]] constexpr Vec operator/(T operand) const {
        return coordinate_wise_operation([operand](T coordinate) -> T {return coordinate / operand;});
    }
    [[nodiscard]] constexpr Vec element_wise_product(Vec const& other) const {
        return coordinate_wise_operation(other, [](T t1, T t2) -> T {return t1*t2;});
    }
    [[nodiscard]] constexpr T dot(Vec const& other) const {
        Vec prod = element_wise_product(other);
        return std::accumulate(prod.coordinates.begin(), prod.coordinates.end(), T{});
    }
    template<typename = void> requires (size == 3)
    [[nodiscard]] constexpr Vec cross(Vec const& other) const {
        return  Vec{{coordinates[1] * other[2] - coordinates[2] * other[1],
                     coordinates[2] * other[0] - coordinates[0] * other[2],
                     coordinates[0] * other[1] - coordinates[1] * other[0]}};
    }
    [[nodiscard]] constexpr bool isOrthogonal(Vec const& other) const {
        return dot(other) == T{};
    }
    [[nodiscard]] constexpr bool isParallel(Vec const& other) const {
        return dot(other) == lenght() * other.lenght(); //TODO: use proportionality instead
    }
    inline friend std::ostream& operator<<(std::ostream& os, Vec const& v) {
        os << '(';
        constexpr std::size_t last = size-1;
        for (std::size_t i = 0; i < last; ++i)
            os << v[i] << ", ";
        os << v[last];
        os << ')';
        return os;
    }
    //TODO: refactor to avoid code duplication
    [[nodiscard]] inline friend constexpr Point_t operator+(Point_t const& start, Vec const& direction) {
        Point_t result = Point_t::ORIGIN();
        for (std::size_t i = 0; i < size; ++i)
            result[i] = start[i] + direction[i];
        return result;
    }
    [[nodiscard]] inline friend constexpr Point_t operator-(Point_t const& start, Vec const& direction) {
        Point_t result = Point_t::ORIGIN();
        for (std::size_t i = 0; i < size; ++i)
            result[i] = start[i] - direction[i];
        return result;
    }
    [[nodiscard]] inline friend constexpr Vec operator*(T factor, Vec const& v) {
        return v*factor;
    }


private:
    std::array<T, size> coordinates{};

    constexpr Vec() = default;

    [[nodiscard]] constexpr Vec coordinate_wise_operation(Vec const& other, std::function<T(T, T)> operation) const {
        Vec result;
        for (std::size_t i = 0; i < size; ++i)
            result[i] = operation(coordinates[i], other[i]);
        return result;
    }
    [[nodiscard]] constexpr Vec coordinate_wise_operation(std::function<T(T)> operation) const {
        Vec result;
        for (std::size_t i = 0; i < size; ++i)
            result[i] = operation(coordinates[i]);
        return result;
    }
};

//TODO other operators

}

#endif //RAY_TRACER_VEC_H
