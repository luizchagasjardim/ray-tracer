//
// Created by luizjardim on 05.09.20.
//

#ifndef RAY_TRACER_DOUBLEUTIL_H
#define RAY_TRACER_DOUBLEUTIL_H

#include <cmath>

namespace ray_tracer::util {

inline constexpr bool double_equal(double x, double y, double tolerance = 0.001) {
    return std::abs(x - y) < tolerance;
}

inline constexpr double PI() {
    return 3.14'15'92'65'35'89'79'32'38'5;
}

template<typename T = double> //TODO remove this default value and rename this file?
inline constexpr T clamp(T min, T value, T max) {
    return value > max ? max : (value < min ? min : value);
}

}

#endif //RAY_TRACER_DOUBLEUTIL_H
