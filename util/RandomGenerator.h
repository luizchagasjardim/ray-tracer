//
// Created by luizjardim on 04.09.20.
//

#ifndef RAY_TRACER_RANDOMGENERATOR_H
#define RAY_TRACER_RANDOMGENERATOR_H

#include "Point.h"
#include "DoubleUtil.h"

#include <random>

namespace ray_tracer::util {

template<typename T, std::size_t size> //TODO: remove size
class RandomGenerator {
    using Point_t = Point<T, size>;
    using Vec_t = Vec<T, size>;
public:
    RandomGenerator() = default;

    [[nodiscard]] T number() {
        return distribution(generator);
    }

    [[nodiscard]] T number(T min, T max) {
        return number() * (max - min) + min;
    }

    [[nodiscard]] Point_t onUnitSphere() {
        //a point on the unit sphere has the form (sin u, cos u sin v, cos u cos v)
        double u = distribution(generator) * 2 * PI();
        double v = distribution(generator) * 2 * PI();
        return Point_t{{std::sin(u), std::cos(u) * std::sin(v), std::cos(u) * std::sin(v)}};
    }

    [[nodiscard]] Point_t inUnitSphere() {
        //a point on the unit sphere has the form (sin u, cos u sin v, cos u cos v)
        double u = distribution(generator) * 2 * PI();
        double v = distribution(generator) * 2 * PI();
        double r = distribution(generator);
        return Point_t{{r * std::sin(u), r * std::cos(u) * std::sin(v), r * std::cos(u) * std::sin(v)}};
    }

    [[nodiscard]] Vec_t unitVector() {
        return Vec_t{Point_t::ORIGIN(), onUnitSphere()};
    }

    [[nodiscard]] Vec_t vectorInsideUnitSphere() {
        return Vec_t{Point_t::ORIGIN(), inUnitSphere()};
    }

private:
    std::random_device randomDevice;
    std::mt19937 generator{randomDevice()};
    std::uniform_real_distribution<T> distribution{0, 1};
};

}
#endif //RAY_TRACER_RANDOMGENERATOR_H
