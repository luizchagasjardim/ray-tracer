//
// Created by luizjardim on 05.09.20.
//

#ifndef RAY_TRACER_STRAIGHTLINE_H
#define RAY_TRACER_STRAIGHTLINE_H

#include "Point.h"
#include "Vec.h"

namespace ray_tracer::util {

template<typename T, std::size_t size>
class StraightLine {
    using Point = ::ray_tracer::util::Point<T, size>;
    using Vec = ::ray_tracer::util::Vec<T, size>;
public:
    constexpr StraightLine(Point point_on_line, Vec line_direction) : point{std::move(point_on_line)}, direction{std::move(line_direction)} {}

    [[nodiscard]] constexpr Point getPoint() const {
        return point;
    }

    [[nodiscard]] constexpr Vec getDirection() const {
        return direction;
    }

    [[nodiscard]] constexpr bool isOn(Point const& p) const {
        Vec aux{point, p};
        return direction.isParallel(p);
    }

    [[nodiscard]] constexpr Point closest_to(Point const& p) const {
        Vec aux{point, p};
        return point + (aux.dot(direction) * direction) / direction.lenght_squared();
    }

    [[nodiscard]] constexpr T distance_squared_to(Point const& p) const {
        return closest_to(p).distance_squared_to(p);
    }

    [[nodiscard]] constexpr T distance_to(Point const& p) const {
        //I could use instead Vec{p, point}.cross(direction.normalized()).length(), but this only works in 3 dimensions
        return closest_to(p).distance_to(p);
    }

    [[nodiscard]] constexpr bool isParallel(StraightLine const& other) const {
        return direction.isParallel(other.direction);
    }

    [[nodiscard]] constexpr bool operator==(StraightLine const& other) const {
        return isParallel(other) && isOn(other.point);
    }

    template<typename = std::enable_if_t<size == 3>> //TODO: generalize?
    [[nodiscard]] constexpr T distance_to(StraightLine const& other) const {
        if (isParallel(other))
            return distance_to(other.point);

        //consider a plane through this->point with directions this->direction and other.direction.
        //this plane must be parallel to other.
        //the distance between this and other is the distance between the plane and other,
        //which in turn is the distance between the plane and other.point.
        //the distance from a point p to a plane through c with normal n (unit vector) is |(p-c).n|
        Vec normal = direction.cross(other.direction).normalized();
        T oriented_distance = Vec{point - other.point}.dot(normal);
        return std::abs(oriented_distance);
    }

private:
    Point point;
    Vec direction;
};

}

#endif //RAY_TRACER_STRAIGHTLINE_H
