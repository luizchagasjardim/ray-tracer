//
// Created by luizjardim on 18.09.20.
//

#ifndef RAY_TRACER_OPTIONS_H
#define RAY_TRACER_OPTIONS_H

#include "Camera.h"
#include "Scene.h"

#include <string>

namespace ray_tracer {

class Options {
    using Scene = ::ray_tracer::scene::Scene;
    using Camera = ::ray_tracer::scene::Camera;
public:
    bool add(std::string const& type, std::string const& value) {
        try {
            if (type == "-w" || type == "--width")
                read_width(value);
            if (type == "-h" || type == "--height")
                read_height(value);
            if (type == "-r" || type == "--rays_per_pixel")
                read_rays_per_pixel(value);
            if (type == "-b" || type == "--ray_bounce")
                read_ray_bounce(value);
            if (type == "-t" || type == "--threads")
                read_number_of_threads(value);
            if (type == "-s" || type == "--scene")
                scene_name = value;
            if (type == "-o" || type == "--output")
                output_file = value;
            return true;
        } catch (std::invalid_argument const&) {
            return false;
        }
    }

    std::optional<Scene> create_scene(Camera camera) {
        if (scene_name == "complete")
            return complete_scene(camera);
        if (scene_name == "cylinder")
            return cilinder_scene(camera);
        if (scene_name == "ellipsoid")
            return ellipsoid_scene(camera);
        return {};
    }

    std::size_t image_width = 1920;
    std::size_t image_height = 1080;
    unsigned rays_per_pixel = 100;
    unsigned ray_bounce = 50;
    unsigned number_of_threads = 9;
    std::string scene_name = "complete";
    std::string output_file = "image.ppm";
private:
    void read_width(std::string const& value) {
        image_width = std::stoi(value);
    }
    void read_height(std::string const& value) {
        image_height = std::stoi(value);
    }
    void read_rays_per_pixel(std::string const& value) {
        rays_per_pixel = std::stoi(value);
    }
    void read_ray_bounce(std::string const& value) {
        ray_bounce = std::stoi(value);
    }
    void read_number_of_threads(std::string const& value) {
        number_of_threads = std::stoi(value);
    }
    Scene complete_scene(Camera);
    Scene cilinder_scene(Camera);
    Scene ellipsoid_scene(Camera);
};

}


#endif //RAY_TRACER_OPTIONS_H
