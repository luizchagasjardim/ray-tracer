//
// Created by luizjardim on 30.08.20.
//

#ifndef RAY_TRACER_IMAGE_H
#define RAY_TRACER_IMAGE_H

#include "Camera.h"
#include "Color.h"
#include "ColorAverage.h"
#include "Ray.h"
#include "Scene.h"
#include "RandomGenerator.h"

#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

namespace ray_tracer {

class Image {
private:
    using Color_t = ::ray_tracer::util::Color;
    using PixelGrid = std::vector<std::vector<Color_t>>;
public:

    explicit Image(PixelGrid image_pixels) : pixels{std::move(image_pixels)}, horizontal_size{pixels.size()},
        vertical_size{horizontal_size > 0 ? pixels[0].size() : 0}{}

    Image& gamma_correction() {
        std::cerr << "\rGamma correction..." << std::flush;
        for (auto& line : pixels)
            for (Color_t& pixel : line)
                pixel = pixel.gammaCorrection();
        std::cerr << "\rGamma correction: finished!\n" << std::flush;
        return *this;
    }

    Image& gamma_correction(double gamma) {
        std::cerr << "\rGamma correction..." << std::flush;
        for (auto& line : pixels)
            for (Color_t& pixel : line)
                pixel = pixel.gammaCorrection(gamma);
        std::cerr << "\rGamma correction: finished!\n" << std::flush;
        return *this;
    }

    Color_t operator()(std::size_t x, std::size_t y) const {
        return pixels[x][y];
    }

    Color_t& operator()(std::size_t x, std::size_t y) {
        return pixels[x][y];
    }

    void write_to_ppm_file(std::string const& path) {
        std::cerr << "\rWriting to ppm file..." << std::flush;
        std::ofstream image;
        image.open(path);
        image << "P3\n";
        image << horizontal_size << ' ' << vertical_size << '\n';
        image << 255 << '\n';
        for (std::size_t j = 0; j < vertical_size; ++j)
            for (std::size_t i = 0; i < horizontal_size; ++i)
                image << pixels[i][j] << '\n';
        image.close();
        std::cerr << "\rWriting to ppm file: finished!\n" << std::flush;
    }

private:
    PixelGrid pixels;
    std::size_t horizontal_size = 1920;
    std::size_t vertical_size = 1080;
};

class ImageRenderer {
private:
    using Color_t = ::ray_tracer::util::Color;
    using ColorAverage_t = ::ray_tracer::util::ColorAverage;
    using PixelLine = std::vector<Color_t>;
    using PixelGrid = std::vector<PixelLine>;
    using Camera_t = ::ray_tracer::scene::Camera;
    using Scene_t = ::ray_tracer::scene::Scene;
    using Random = ::ray_tracer::util::RandomGenerator<double, 3>;
public:

    explicit ImageRenderer(Scene_t image_scene) : scene{std::move(image_scene)} {}

    ImageRenderer& with_size(std::size_t horizontal, std::size_t vertical) {
        horizontal_size = horizontal;
        vertical_size = vertical;
        lines_remaining = horizontal_size;
        return *this;
    }

    ImageRenderer& with_rays_per_pixel(unsigned u) {
        rays_per_pixel = u;
        return *this;
    }

    ImageRenderer& with_ray_bounce(unsigned u) {
        ray_bounce = u < 2 ? 2 : u;
        return *this;
    }

    ImageRenderer& with_number_of_render_threads(unsigned u) {
        number_of_render_threads = u == 0 ? 1 : u;
        return *this;
    }

    Image render();

private:
    Scene_t scene;
    std::size_t horizontal_size = 1920;
    std::size_t vertical_size = 1080;
    unsigned rays_per_pixel = 100;
    unsigned ray_bounce = 50;
    Random random;
    unsigned number_of_render_threads = 9;
    unsigned lines_remaining = horizontal_size;

    PixelGrid render_image();

    void render_strip(PixelGrid& pixels, unsigned thread_number);

    PixelLine render_line(std::size_t i);

    [[nodiscard]] Color_t render_pixel(std::size_t i, std::size_t j);

    [[nodiscard]] Color_t render_ray(std::size_t i, std::size_t j);

    void print_progress();

    void print_finished();
};

}

#endif //RAY_TRACER_IMAGE_H
